const co = require('co');
const sqlite3 = require('sqlite3').verbose();
const { db_path } = require('./settings_loc.js');
const db = new sqlite3.Database(db_path);

db.configure('busyTimeout', 60000);

function get(...args) {
  return new Promise((res, rej) => {
    db.get(...args, (e, r) => !!e ? rej(e) : res(r));
  });
}

function all(...args) {
  return new Promise((res, rej) => {
    db.all(...args, (e, r) => !!e ? rej(e) : res(r));
  });
}

function getTotalInfo(lot) {
  return co.wrap(function*() {
    let message = `Завершился лот ${lot.desc}. Победителем лота стал #`;
    const query = `SELECT p, (SELECT nick FROM users WHERE bids.user_id = users.id) as user FROM "bids" WHERE lot_id=${lot.lot_id} ORDER BY t DESC LIMIT 2`;
    const result = yield all(query);
    if(result) {
      message += `${result[0].user}. Товар обойдется ему в ${result[0].p} руб.\n`;
      message += 'Теперь немного статистики на количество ставок:\n';

      const query2 = `SELECT (SELECT nick FROM users WHERE bids.user_id = users.id) as user, count(user_id) as bids_count FROM bids WHERE lot_id=${lot.lot_id} GROUP BY user_id ORDER BY bids_count DESC`;
      const result2 = yield all(query2);
      if(result2) {
        const winner = result2.find(r => r.user == result[0].user);
        message += `1. Победитель сделал ставок - ${winner.bids_count}\n`;
        message += `2. Больше всех ставок сделал #${result2[0].user} - ${result2[0].bids_count}\n`;

        const last_rival = result2.find(r => r.user == result[1].user);
        message += `3. Соперник победителя под ником #${result[1].user} - ${last_rival.bids_count}\n`;
        message += 'Список игроков, сделавших больше 100 ставок:\n';

        result2.forEach(res => {
          if(res.bids_count >= 100) message += `#${res.user} ${res.bids_count}\n`;
        });

        message += '#bonusmall #бонусмол\n';

        const query3 = `SELECT url FROM goods WHERE id=(SELECT good_id FROM lots_history WHERE lot_id=${lot.lot_id})`;
        const result3 = yield get(query3);
        message += `bonusmall.ru/product/${result3.url}`;

        return message;
      } else return `Can not get bids count for lot ${lot.lot_id}: ${lot.desc}`;
  } else return `There are no total information about lot ${lot.lot_id}: ${lot.desc}`;
  })();
}

module.exports = {
  get,
  all,
  getTotalInfo
}