const co = require('co');
const { get, getTotalInfo } = require('./db_functions.js');
const { downloadImages } = require('./download_images.js');
const { vkPublicPost } = require('./vk.js');
const got = require('got');
const cheerio = require('cheerio');

module.exports = {
  sub(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    const lotIds = match[1].split(' ').filter(id => !!id);

    lotIds.forEach(l => subLots[l] = { id: l });
    const message = 'You have subscribed successfully on this lot(s)!';

    bot.sendMessage(fromId, message);
  },
  unsub(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    const lotIds = match[1].split(' ').filter(id => !!id);

    lotIds.forEach(l => { if(subLots[l]) delete subLots[l] });
    const message = 'You have unsubscribed successfully on this lot(s)!';

    bot.sendMessage(fromId, message);
  },
  showSubs(bot, msg, match, subLots) {
    const fromId = msg.from.id;
    let message = '';
    for(let key in subLots) {
      message += `${subLots[key].id}: ${subLots[key].desc}\n` 
    }
    bot.sendMessage(fromId, message);
  },
  async public(bot, msg, match) {
    const fromId = msg.from.id;
    const lotId = match[1];
    try {
      const lot = await get(`SELECT * FROM lots_history, goods WHERE lot_id=${lotId} AND lots_history.good_id=goods.id`);
      console.log('get lot', lot);

      const postText = await getTotalInfo(lot);
      console.log('get postText');
      const imgsPaths = await getDownloadedGoodImagesPaths(lot.url);
      console.log('get imgsPaths', imgsPaths);

      const result = await publicPost(postText, imgsPaths);
      console.log('send post', result);

      bot.sendMessage(fromId, result);
    } catch(err) {
      bot.sendMessage(fromId, err.message);
    }
  }
}

async function publicPost(postText, imgsPaths) {
  try {
    await vkPublicPost(postText, imgsPaths);
  } catch(err) {
    throw err;
  }
  return 'Post was published successfully.';
}

async function getGoodImagesUrls(url) {
  const imgs = [];
  const goodUrl = 'https://bonusmall.ru/product/' + url;

  try {
    const res = await got(goodUrl, { timeout: 5000 });
    const body = res.body;

    if(/403 Forbidden/.test(body)) throw new Error('Forbidden error');

    const $ = cheerio.load(body);
    $('.picture-item').each((i, el) => {
      const src = $(el).children('img').attr('src');
      imgs.push('https://bonusmall.ru' + src);
    });
  } catch(err) {
    throw err;
  }

  return imgs;
}

async function getDownloadedGoodImagesPaths(url) {
  try {
    const imgsUrls = await getGoodImagesUrls(url);
    const imgsPaths = await downloadImages(imgsUrls);

    return imgsPaths;
  } catch(err) {
    throw err;
  }
}