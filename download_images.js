const https = require('https');
const path = require('path');
const fs = require('fs');

async function downloadImages(imgsUrls) {
  const imgsPaths = [];
  for(let i = 0; i < imgsUrls.length; i++) {
    try {
      const imgPath = __dirname + '/images/img' + i + '.png';
      await getAndWrite(imgsUrls[i], imgPath);

      imgsPaths.push(imgPath);
    } catch(err) {
      throw err;
    }
  }

  return imgsPaths;
}

module.exports = {
  downloadImages
}

function getAndWrite(url, imgPath) {
  return new Promise((res, rej) => {
    https.get(url, (result) => {
      let imgData = '';
      result.setEncoding('binary');

      result.on('data', (chunk) => {
        imgData += chunk;
      });

      result.on('end', () => {
        if(/html/.test(imgData)) {
          console.log('Forbidden error', imgData);
          rej('Forbidden error');
        }

        fs.writeFileSync(imgPath, imgData, { encoding: 'binary', flag: 'a' });
        res();
      });
    });
  })
}